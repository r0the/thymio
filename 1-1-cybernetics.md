# Kybernetik
---

Kybernetik ist nach ihrem Begründer Norbert Wiener die Wissenschaft der Steuerung und Regelung von Maschinen, lebenden Organismen und sozialen Organisationen und wurde auch mit der Formel «die Kunst des Steuerns» beschrieben.[^1]

## Begriff

Der Begriff «Kybernetik» stammt vom griechischen κυβερνήτης, was Steuermann bedeutet. Der französische Physiker und Mathematiker André-Marie Ampère hat 1834 in seinem Essay *sur la philosophie des sciences* das Wort *cybernétique* verwendet um die Wissenschaft des Regierens zu beschreiben. Der Begriff wurde 1948 von Robert Wiener in seinem Artikel *Cybernetics or Control and Communication in the Animal and the Machine* übernommen.[^2]

## Grundidee

Ein kybernetisches System reagiert auf Reize oder Eingaben aus seiner Umgebung, auf welche es auf eine bestimmte Weise reagiert. Dadurch verändern sich die Reize, was wiederum zu einer Veränderung des Verhaltens führt. Dadurch entsteht eine Rückkopplungsschleife.

Der Thermostat ist ein typisches Beispiel eines kybernetischen Systems. Ein Thermometer (Sensor) misst die Raumtemperatur. Die Steuerung vergleicht die gemessene Temperatur mit der eingestellten Solltemperatur und schaltet die Heizung entsprechend ein oder aus. Dies wiederum beeinflusst die Raumtemperatur.

![Thermostat als Beispiel eines kybernetischen Systems](images/cybernetics-thermostat.svg)

Auf die gleiche Weise kann ein Roboter als kybernetisches System betrachtet werden. Ein Roboter nimmt beispielsweise seine Position im Raum mittels Sensoren war. Darauf reagiert die Steuerung, indem sie mittels Aktoren (z.B. Motoren) die Position verändert, um eine vorgegebene Zielposition zu erreichen.

![Roboter als kybernetisches System](images/cybernetics-robot.svg)

## Anwendungen

Kybernetik ist eine typischen interdisziplinäre Wisschenschaft, welche in vielen Bereichen eingesetzt wird:

- Biologie: Anpassung von Organismen an die Umwelt
- Informatik: Steuerungssysteme für Maschinen, Roboter, Nachrichtenübertragung
- technische Wissenschaften: Analyse von Systemfehlern
- Betriebswirtschaft
- Mathematik: Kontrolltheorie, Systemtheorie
- Psychologie: Bindungstheorie

## Cyborg

Der Begriff Cyborg oder Kyborg bezeichnet ein Mischwesen aus lebendigem Organismus und Maschine. Zumeist werden damit Menschen beschrieben, deren Körper dauerhaft durch künstliche Bauteile ergänzt werden. Der Name ist ein Akronym, abgeleitet vom englischen *CYBernetic ORGanism*. Da Cyborgs technisch veränderte biologische Lebensformen sind, sollten sie nicht mit Androiden oder anderen Robotern verwechselt werden.[^3]

Der Begriff stammt aus dem Kontext der Raumfahrt in den 1960er Jahren. Mit Hilfe von biochemischen, physiologischen und elektronischen Modifikationen sollten Menschen als «selbstregulierende Mensch-Maschinen-Systeme» im Weltraum überlebensfähig sein.[^3]

[^1]: Quelle: [Wikipedia](https://de.wikipedia.org/wiki/Kybernetik)
[^2]: Quelle: [Wikipedia](https://en.wikipedia.org/wiki/Cybernetics)
[^3]: Quelle: [Wikipedia](https://de.wikipedia.org/wiki/Cyborg)
