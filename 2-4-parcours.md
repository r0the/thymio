# Aufgaben
---

::: exercise Parcours

Versuche, auf dem Parcours (Schneelandschaft mit einer breiten, schwarzen Strasse) zuverlässig eine Runde zu fahren. Der Roboter soll also der Strasse folgen, ohne sich von anderen Objekten (z.B. den Häusern) ablenken zu lassen. Sobald dies zuverlässig gelingt, kannst du versuchen, deine Rundenzeit zu verbessern.
:::

::: extra Zusatzaufgabe «Tischkante»
Der Roboter soll auf einem Tisch der Kante entlang fahren – natürlich, ohne herunterzufallen – und dabei nicht kreuz und quer über den Tisch fahren.

**Beachte**: Keiner der Roboter kann haargenau geradeaus fahren und sich um exakt 90° drehen!
:::

::: extra Zusatzaufgabe «Folgen»

Versucht zu zweit folgendes Experiment: Der eine Roboter soll (z.B. mit den Tasten der Fernsteuerung gesteuert) vor dem zweiten Roboter herfahren. Der zweite Roboter soll dem ersten möglichst gut folgen, darf ihn aber nicht rammen.
:::
