# Globale Ereignisse
---

## Ereignis definieren

Für die Übertragung von Werten im Aseba-Netzwerk müssen globale Ereignisse definiert werden. Das Ereignis erhält einen Namen. Ausserdem muss die Anzahl Werte definiert werden, die dem Ereignis mitgeliefert werden.

Als Beispiel sollen die Messwerte des Beschleunigungsmessers grafisch dargestellt werden. Als erstes wird mit der Schaltfläche :mdi-plus: unter **Globale Ereignisse** ein neues Ereignis definiert.

![Hinzufügen eines globalen Ereignisses](images/create-global-event.png)

Das Ereignis soll **graph** heissen und drei Werte (*Argumente*) übermitteln.

## Ereignis senden

Im Programm des Roboters wird das Ereignis mit der `emit`-Anweisung ausgelöst, wenn eine neue Messung der Beschleunigung vorliegt:

``` aseba
onevent acc
    emit graph acc
```

Die Länge des angegebenen Vektors muss mit der vorhin angegebenen Zahl der Argumente übereinstimmen. Der Vektor `acc` hat drei Elemente.

## Diagramm

Aseba Studio bietet die Möglichkeit, Werte grafisch darzustellen. Das ist besonders hilfreich, um die Messwerte der Sensoren in bestimmten Situationen zu beobachten.

![grafische Darstellung von Werten in Aseba Studio](images/plot.png)


## Werte aufzeichnen

Nachdem das Programm auf dem Thymio gestartet wird, werden die Ereignisse mit den aktuellen Messwerten an Aseba Studio übermittelt. Sie werden im Bereich unten rechts dargestellt.

Durch einen Rechtsklick auf ein globales Ereignis und Auswahl des Menüpunkts **Ereignis ... aufzeichnen** wird die Aufzeichnung der Werte gestartet.
