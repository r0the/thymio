# VPL – *Visual Programming Language*
---

![Übersicht Thymio VPL ©](images/vpl-gui.svg)

## Ereignis-Aktions-Paare

Um dem Thymio Befehle zu geben, definiert man Paare aus Ereignisse und Aktionen (Wenn-Dann-Blöcke). Ein Ereignis kann – wie man im Screenshot oben sieht – eine oder mehrere Aktionen auslösen. Das Programm wird nicht wie sonst üblich von oben nach unten abgearbeitet, sondern die Ereignisse bestimmen, welche Aktionen ausgeführt werden.
