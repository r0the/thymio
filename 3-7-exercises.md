# Zusatzaufgaben
---

## Arbeiten mit dem Timer und mit Zuständen
1. Der Roboter soll ein Quadrat abfahren, d.h. er soll ca. 1s vorwärts fahren, sich dann um ca. 90° drehen und so weiter.
1. Teste dein Programm mit einem anderen Roboter und erkläre, was nun passiert.
1. Der Roboter soll kreuz und quer auf dem Tisch fahren, ohne herunterzufallen.
   **Achtung**: Bitte lass den Roboter nie alleine auf dem Tisch fahren! Sei stets bereit, ihn aufzufangen, für den Fall, dass dein Programm nicht korrekt funktioniert.
   **Hinweis**: Um die Gefahr von Unfällen zu minimieren, sollte der Roboter nur mit mittlerer Geschwindigkeit fahren, da er sonst unter Umständen nicht rechtzeitig auf die Tischkante reagieren kann.


## Verhalten
1. Versuche noch einmal, dem Roboter ein **neugieriges** Verhalten zu programmieren. Er soll seine Umgebung (auf einem Tisch, die Umgebung ist somit klar begrenzt!) erkunden und solange fahren, bis er ein Hindernis findet. Sobald er am Ziel ist, soll er anhalten und seinen Erfolg mit einer Farbe anzeigen.
