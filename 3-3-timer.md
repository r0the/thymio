# Der Timer
---

Im fortgeschrittenen Modus kann als zusätzliche Aktion der Timer gestellt werden. Dazu verwendet man das Wecker-Symbol bei den blauen Aktionselementen. Anschliessend bewegt man den Zeiger des Timers im Uhrzeigersinn, um die gewünschte Zeit einzustellen. Eine ganze Umdrehung entspricht 4s, ein Viertel der Uhr entspricht 1s.

Das orange Ereignis-Symbol mit dem klingelnden Wecker wird eingesetzt, um nach Ablauf der eingestellten Zeit des Timers eine neue Aktion einzuleiten.

## Ein einfaches Beispiel

- Durch Antippen der vorwärts-Taste fährt der Thymio los. In gleichen Moment wird der Timer auf 1s gestellt.
- Nach Ablauf der eingestellten Zeit (1s) bleibt der Thymio stehen.
- Dieses Verhalten kann wiederholt werden, wenn erneut auf die vorwärts-Taste getippt wird.

![](images/example-timer-1.png)
