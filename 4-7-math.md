# Mathematik
---

Aseba stellt einige Subroutinen für mathematische Operationen zu Verfügung. Die Subroutienen sind grundsätzlich für Vektoren ausgelegt, sie funktionieren jedoch auch mit einzelnen Werten.

Das folgende Beispiel zeigt, wie die Vektoren `a` und `b` addiert werden. Das Resultat wird im Vektor `r` abgelegt:
``` aseba
var a[2] = [1, 2]
var b[2] = [2, 3]
var r[2]
call math.add(r, a, b) # r == [3, 5]
```

In Gegensatz zu anderen Programmiersprachen wie Python wird in Aseba die Variable, in welcher das Resultat gespeichert werden soll, als weiterer Parameter übergeben. Dabei kann für das Resultat auch eine Variable verwendet werden,

``` aseba
var a[2] = [1, 1]
var b[2] = [2, 3]

call math.add(a, b, a)
```

## Grundoperationen

~~~ aseba
call math.add(r, a, b)
~~~
berechnet $r_i = a_i + b_i$.

~~~ aseba
call math.addscalar(r, a, k)
~~~
berechnet $r_i = a_i + k$.

~~~ aseba
call math.sub(r, a, b)
~~~
berechnet $r_i = a_i - b_i$.

~~~ aseba
call math.mul(r, a, b)
~~~
berechnet $r_i = a_i \cdot b_i$.

~~~ aseba
call math.div(r, a, b)
~~~
berechnet $r_i = \frac{a_i}{b_i}$. Dabei darf kein $b_i$ Null sein.

~~~ aseba
call math.muldiv(r, a, b, c)
~~~
berechnet $r_i = \frac{a_i \cdot b_i}{c_i}$. Dabei darf kein $c_i$ Null sein.

~~~ aseba
call math.sqrt(r, a)
~~~
berechnt $r_i = \sqrt{a_i}$. Dabei darf kein $a_i$ negativ sein. Die Resultate werden grundsätzlich abgerundet.

``` aseba
var a[3] = [3, 5, 10]
var r[3]

call math.sqrt(r, a) # r == [1, 2, 3]
```

## Minima und Maxima

~~~ aseba
call math.min(r, a, b)
~~~
speichert für jedes $i$ das Minimum von $a_i$ und $b_i$ in $r_i$, also $r_i = \min(a_i, b_i)$.

~~~ aseba
call math.max(r, a, b)
~~~
speichert für jedes $i$ das Maximum von $a_i$ und $b_i$ in $r_i$, also $r_i = \max(a_i, b_i)$.

~~~ aseba
call math.stat(a, min, max, mean)
~~~
berechnet das Minimum, das Maxiumum sowie den Mittelwert des Vektors $\vec{a}$.

~~~ aseba
call math.argbounds(a, i, j)
~~~
bestimmt die Indizes des kleinsten und grössten Elements im Vektor $\vec{a}$. Es gilt also $a_{i} = \min(a_0, a_1,\ldots,a_{n-1})$ und $a_j = \min(a_0, a_1,\ldots,a_{n-1})$

``` aseba
var a[] = [90, 40, 70, 30, 90, 20]
var min
var max
call math.argbounds(a, min, max) # min == 5, max == 0
```

~~~ aseba
call math.clamp(r, a, b, c)
~~~
beschränkt jedes Element $b_i$ auf den Bereich $[c_i, d_i]$. Jedes Element des resultierenden Vektors $\vec{r}$ wird so bestimmt:

$$r_i = \begin{cases}
c_i &\text{falls } b_i < c_i \\
d_i &\text{falls } b_i > d_i \\
b_i &\text{sonst} \\
\end{cases}$$

## Weitere Subroutinen

~~~ aseba
call math.fill(r, k)
~~~
füllt den Vektor $\vec{r}$ mit dem Wert $k$, also $r_i = k$.

~~~ aseba
call math.copy(r, a)
~~~
kopiert den Vektor $\vec{a}$ in den Vektor $\vec{r}$, also $r_i = a_i$.

~~~ aseba
call math.sort(r)
~~~
sortiert den Vektor $\vec{r}$.

~~~ aseba
call math.rand(r)
~~~
bestimmt einen zufälligen Wert $r$ aus dem Bereich $-32768$ bis $32767$.

## Geometrie

::: warning
Bei den folgenden Funktionen ist zu beachten, dass in Aseba mangels Fliesskommazahlen Winkel auf eine spezielle Weise dargestellt werden.

Da keine offizielle Dokumentation vorhanden ist, wird ohne Verständnis des [Quellcodes][1] von einer Benutzung der folgenden Subroutinen abgeraten.
:::

~~~ aseba
call math.sin(r, a)
~~~
berechnet $r_i = \sin(a_i)$.

~~~ aseba
call math.cos(r, a)
~~~
berechnet $r_i = \cos(a_i)$.

~~~ aseba
call math.atan2(r, x, y)
~~~
berechnet $r_i = \arctan\left(\frac{y_i}{x_i}\right)$.

~~~ aseba
call math.rot2(r, a, w)
~~~
rotiert den zweidimensionalen Vektor $\vec{a}$ um den Winkel $w$ und liefert das Ergebnis als Vektor $\vec{r}$ zurück.

~~~ aseba
call math.dot(r, a, b, n)
~~~
berechnet das Skalarprodukt der Vektoren $\vec{a}$ und $\vec{b}$ geteilt durch $2^n$: $r = \frac{1}{2^n} \cdot \sum_i (a_i \cdot b_i)$

[1]: https://github.com/aseba-community/aseba/blob/23366dafbdbbd3c9b02d951f38ac0a350e3c0dc8/aseba/vm/natives.c
