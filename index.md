# Robotik mit Thymio
---

![](images/cover.1280.jpg)

Thymio ist ein Roboter, der an der École polytechnique fédérale de Lausanne (EPFL) in Zusammenarbeit mit der École cantonale d'art de Lausanne (ECAL) für didaktische Zwecke konzipiert worden ist.

* [:link: Offizielle Webseite von Thymio](https://www.thymio.org/)

Thymio ist **Open Source**, das heisst dass die Baupläne des Roboters frei verfügbar sind und dass jeder den Roboter selbst nachbauen darf. Auch die Software, mit welcher Thymio betrieben wird, ist frei verfügbar.

::: cards 3
[![](images/icon-vpl.svg)](?page=2-0-vpl)
#### [Thymio VPL](?page=2-0-vpl)
Die Visual Programming Language (VPL) eignet sich für Einsteiger ohne Programmiererfahrung.

***
[![](images/icon-aseba-studio-thymio.svg)](?page=3-0-aseba)
#### [Aseba Studio](?page=3-0-aseba)
Aseba ist eine textbasierte Programmiersprache, mit welcher alle Features des Thymio-Roboters ausgenutzt werden können.

***
[![](images/icon-aseba-playground.svg)](?page=simulation)
#### [Playground](?page=simulation)
Simulation von Thymio-Robotern auf dem Computer
:::

<!--
[![](images/icon-net-config.svg)](?page=thymio.config)

#### [Konfiguration](?page=thymio.config)

Konfiguration des Roboters und des Funknetzwerks
-->
