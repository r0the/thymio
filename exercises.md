# Aufgabensammlung
---

::: exercise K.I.T.T / Cylon

Programmiere mit den Leuchtdioden der vorderen Distanzsensoren ein hin- und herlaufendes Licht.

***
```aseba
var led[6]
var i
var pos = 0
timer.period[0] = 1

onevent timer0
    for i in 0:5 do
        if i == pos or i == 11 - pos then
            led[i] = 32
        else
            led[i] = 0
        end
    end
    call leds.prox.h(led[0], led[1], led[2], led[3], led[4], led[5], 0, 0)
    pos++
    pos = pos % 12
    timer.period[0] = 100
```
:::

::: exercise Stimmungslicht
Verwandle den Thymio in ein steuerbares Stimmungslicht. Die Farbe der farbigen Leuchtdioden soll durch Tasten geregelt werden können.

- Mit den Tasten :thymio-up: und :thymio-down: wird der Grünanteil der Farbe eingestellt.
- Mit den Tasten :thymio-right: und :thymio-left: wird der Blauanteil der Farbe eingestellt.

**Zusatz:** Da nur vier Tasten zu Verfügung stehen, sollen die zwei hinteren Distanzsensoren als zusätzliche Tasten verwendet werden, um den Rotanteil der Farbe zu regeln. Sie gelten als gedrückt, wenn ein Objekt (der Finger) vor den Sensor gehalten wird.

***
```aseba
var red = 0
var green = 16
var blue = 16

call leds.prox.h(0, 0, 0, 0, 0, 0, 0, 0)
call leds.prox.v(0, 0)
call leds.temperature(0, 0)
callsub updateColor

sub updateColor
    call leds.top(red, green, blue)
    call leds.bottom.left(red, green, blue)
    call leds.bottom.right(red, green, blue)

onevent button.forward
    green = green + 2
    if green > 31 then
        green = 31
    end
    callsub updateColor

onevent button.backward
    green = green - 2
    if green < 0 then
        green = 0
    end
    callsub updateColor

onevent button.right
    blue = blue + 2
    if blue > 31 then
        blue = 31
    end
    callsub updateColor

onevent button.left
    blue = blue - 2
    if blue < 0 then
        blue = 0
    end
    callsub updateColor

onevent prox
    if prox.horizontal[6] > 2000 then
        red = red + 2
        if red > 31 then
            red = 31
        end
        callsub updateColor
    end
    if prox.horizontal[5] > 2000 then
        red = red - 2
        if red < 0 then
            red = 0
        end
        callsub updateColor
    end
```
:::

::: exercise Fernsteuerung
Programmiere mit zwei Thymios ein ferngesteuertes Auto. Der einie Thymio ist das Auto, der andere die Fernsteuerung. Für die Fernsteuerung gibt es folgende Varianten:

**Variante 1**: Die Fahrbefehle werden mit den Richtungstasten auf der Fernsteuerung gegeben.

**Variante 2**: Die Fahrbefehle werden durch die Orientierung im Raum der Fernsteuerung gegeben. Wir sie nach vorne geneigt, fährt das Auto vorwärts. Wird sie nach links oder rechts geneigt, fährt das Auto eine Kurve in die entsprechende Richtung.

Die Fahrbefehle werden als globale Ereignisse an das Auto übermittelt.
***

Programm der Fernsteuerung:
```aseba
var ledL = 0
var ledR = 0

onevent acc
    ledF = 0
    ledL = 0
    ledR = 0
    if acc[0] &lt; -5 then
        ledR = 32
        emit right
    elseif acc[0] > 5 then
        ledL = 32
        emit left
    end
    if  acc[2] > 15 then
        ledF = 32
        emit forward
    end
    call leds.circle(ledF, 0, ledR, 0, 0, 0, ledL, 0)
```

Programm des ferngesteuerten Autos:

```aseba
var ledF = 0
var ledL = 0
var ledR = 0
timer.period[0] = 100

onevent timer0
    call leds.circle(ledF, 0, ledR, 0, 0, 0, ledL, 0)
    if ledF > 0 then
        motor.left.target = 500
        motor.right.target = 500
    else
        motor.left.target = 0
        motor.right.target = 0
    end

    if ledL > 0 then
        motor.left.target -= 300
        motor.right.target += 300
    elseif ledR > 0 then
        motor.left.target += 300
        motor.right.target -= 300
    end
    ledL = 0
    ledR = 0
    ledF = 0
    timer.period[0] = 100


onevent left
    ledL = 32

onevent right
    ledR = 32

onevent forward
    ledF = 32

```
:::

## Einzelner Roboter - Bewegung



#### Ausweichen

Der Roboter soll die Umgebung erkunden und Hindernissen ausweichen. Ab und zu soll er zufällig die Richtung ändern.

#### Abgrund

Der Roboter soll Abgründe erkennen und nicht hineinfallen. Ein Abgrund kann auch durch eine schwarzen Bereich simuliert werden.

#### Linie folgen

Der Roboter soll einer schwarzen Linie folgen.


## Mehrere Roboter

#### Gänsemarsch

Mehrere Roboter sollen einander folgen. Das Verhalten des führenden Roboters besteht aus Ausweichen und Abgrund vermeiden.

#### Catch me if you can («Fangis»)

Mehrere Roboter sollen «Fangis» spielen. Ein Roboter ist der Jäger, er muss einen anderen rammen. Dann wird der gerammte der Jäger. Der Roboter, der gerade Jäger war, ist 30 Sekunden lang «immun».

**Tipp:** Die Roboter melden über Infrarot-Kommunikation ihren aktuellen Status (Jäger, Beute, immun).
