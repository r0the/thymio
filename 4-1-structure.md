# Grundstruktur
---

Ein Aseba-Programm hat immer folgende Grundstruktur:

1. Definition der Variablen
2. Befehle beim Programmstart
3. Definition von Subroutinen
4. Definition von Ereignissen

Die Grundstruktur wird in folgendem Beispiel illustriert:

``` aseba
# 1.  Definition der Variablen
var cmd = 2

# 2. Befehle beim Programmstart
call leds.top(0, 0, 0)

# 3. Definition von Subroutinen
sub updateMotors
  if cmd == 1 then
    motor.left.target = -150
    motor.right.target = 150
    call leds.circle(0, 0, 32, 0, 0, 0, 0, 0)
  elseif cmd == 2 then
    motor.left.target = 300
    motor.right.target = 300
    call leds.circle(32, 0, 0, 0, 0, 0, 0, 0)
  elseif cmd == 3 then
    motor.left.target = 150
    motor.right.target = -150
    call leds.circle(0, 0, 0, 0, 0, 0, 32, 0)
  end

# 4. Definition von Ereignissen
onevent prox
  if prox.horizontal[0] > 2000 then
    cmd = 3
  elseif prox.horizontal[2] > 1000 then
    cmd = 3
  elseif prox.horizontal[4] > 3500 then
    cmd = 1
  else
    cmd = 2
  end
  callsub updateMotors

onevent tap
  call leds.top(32, 0, 0)

```
