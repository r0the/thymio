# Kommunikation
---

## Lokale Kommunikation

Thymio kann seine horizontalen Infrarot-Abstandssensoren dazu verwenden, um einen Zahlenwert zu anderen Robotern in einem Bereich von etwa 15 cm zu senden (Peer to Peer-Kommunikation). Dieser Wert wird mit 10 Hz gesendet, während die Abstandssensoren normal weiterarbeiten. Der Thymio sendet einen 10-Bit-Wert. Um die Kommunikation zu verwenden, rufen Sie die Funktion `prox.comm.enable` auf, mit 1 um die Kommunikation zu ermöglichen und mit 0 um sie auszuschalten. Wenn die Kommunikation aktiviert ist, wird der Wert in der Variablen `prox.comm.tx` alle 100 ms gesendet. Wenn Thymio einen Wert empfängt, wird das Ereignis prox.comm ausgelöst und der Wert in der Variablen `prox.comm.rx` abgelegt.
