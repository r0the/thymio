# Aseba
---

Aseba Studio ist eine Programmierumgebung für den Thymio-Roboter.

![Übersicht Aseba Studio ©](images/aseba-studio.svg)

## Laden und Ausführen

Der linke Bereich des Programmfensters bezieht sich auf den verbundenen Roboter. Mit den Schaltflächen oben links kann ein Programm auf den Roboter geladen und ausgeführt werden:

- **Laden:** Das aktuelle Programm wird auf den Roboter geladen.
- **Ausführen:** Die Ausführung zuletzt geladenen Programms auf dem Roboter wird gestartet.
- **Pause:** Die Ausführung des Programms auf dem Roboter wird angehalten. Achtung: Der letzte Fahrbefehl bleibt erhalten, der Roboter stoppt nicht.

## Variablen

Darunter werden die Werte aller Variablen angezeigt. Hier können Sensorwerte abgelesen werden. Mit einem Klick auf «aktualisieren» werden die Werte neu aus dem Roboter gelesen. Wenn bei «auto» ein Haken gesetzt wird, werden die Werte automatisch in regelmässigen Abständen aktualisiert.

## Stopp

Links unten befindet sich die wichtige Schaltfläche «Thymio stoppen», mit welcher der Roboter sofort angehalten werden kann.

Mit der darunterliegenden Schaltfläche kann die visuelle Programmieroberfläche gestartet werden.

## Programm

Im mittleren Bereich des Fensters wird das Programm geschrieben.

## globale Ereignisse

Hier können globale Ereignisse definiert und überwacht werden.
