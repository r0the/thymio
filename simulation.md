# Simulation
---

Mit dem Programm **Aseba Playground** kann eine Simulation von Thymio-Robotern gestartet werden.

## Auswahl des Szenarios

Beim Start von Aseba Playground muss eine Szenario-Datei ausgewählt werden. Dies Szenarien befinden sich im Ordner **C: > Programme (x86) > AsebaStudio > examples**.

![](images/playground-open.png)

## Szenarien

Die wichtigsten Szenarien werden hier vorgestellt:

::: columns 3
![**r2t2-complete**<br>16 Thymios, 4 bewegliche Tore](images/playground-r2t2-complete.png)
***
![**r2t2-practice**<br>4 Thymios, 1 bewegliches Tor](images/playground-r2t2-practice.png)
***
![**thymio**<br>1 Thymio](images/playground-thymio.png)
***
![**thymio-challenge-pack**<br>1 Thymio, 1 beweglicher Block](images/playground-thymio-challenge-pack.png)
***
![**thymio-discovery-yeti**<br>1 Thymio, Poster](images/playground-thymio-discovery-yeti.png)
***
![**thymio-maze-6x6**<br>2 Tyhmios](images/playground-thymio-maze-6x6.png)
:::

## Unterschiede zum echten Roboter

In der Simulation gibt es folgende Unterschiede zum echten Roboter:

- Die Bodensensoren messen kein Umgebungslicht.
- Es wird keine Beschleunigung gemessen.
- Die Temperatur beträgt immer 22° C.
- Der Infrarot-Empfänger wird nicht simuliert.
- Infrarot-Kommunikation wird nicht simuliert.
- Sound wird nicht simuliert (Mikrofon und Abspielen).
- Die Leuchtdioden bei Temperatursensor, Mikrofon und Bodensensoren werden nicht simuliert.
- Ein `tap`-Ereignis wird ausgelöst, wenn der Roboter mit einem Gegenstant kollidiert oder wenn er mit der Maus angeklickt wird.
