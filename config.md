# Konfiguration
---

## Konfigurationsmodus

Über ein Konfigurationsmodus können verschiedene Einstellungen des Thymio geändert werden.

Um in den Konfigurationsmodus zu gelangen, müssen in eingeschaltetem Zustand die Taten :thymio-left: und :thymio-right: gleichzeitig mindestens drei Sekunden lang gedrückt werden. Den Konfigurationsmodus erkennt man daran, dass die Leuchtdioden der Abstandssensoren blinken.

Durch Drücken der Tasten :thymio-left: und :thymio-right: können die verschiedenen Punkte im Menü durchgeblättert werden. Jeder Punkt wird durch eine Farbe repräsentiert:

- Rot: Lautstärke
- Grün: Motorkalibrierung
- Violett: Wireless-Pairing

Durch Drücken der Taste :thymio-middle: wird der ausgewählte Menüpunkt aktiviert und wieder verlassen.

## Funknetzwerk

Die drahtlose Verbindung des Thymio basiert auf dem 802.15.4-Protokoll im 2.4 GHz-Frequenzband. Mit diesem Protokoll kann ein Netzwerk mit mehreren Knoten (d.h. mehreren Robotern) ohne zentrale Basisstation aufgebaut werden. Somit können Thymios im selben Netzwerk Nachrichten austauschen, ohne dass ein Computer alles koordiniert. Mit dem Wireless-Dongle wird der Computer Teil dieses Netzwerks.

Damit Thymio-Roboter und Dongles miteinander kommunizieren können, müssen alle

- den gleichen **Kanal** benutzen,
- die gleiche **Netzwerkidentifizierung** haben und
- eine unterschiedliche **Knotenidentifizierer** haben.

## Funknetzwerk konfigurieren

::: warning
#### :warning: Knotenidentifizierer stehen lassen
Ändere den Knotenidentifizierer auf keinen Fall, ausser du weisst, was du tust.
:::

![](images/network-config.png)

1. Computer:
  - Stecke den Dongle am Computer ein.
  - Öffne den «Wireless Thymio Netzwerkkonfigurator».
  - Wähle den **Kanal**.
  - Wähle die **Netzwerkidentifizierung**.
  - Klicke auf «Paarung aktivieren».

2. Thymio:
  - Starte den Thymio.
  - Versetze den Thymio in den Wireless-Pairing-Modus (siehe oben).
  - Der Dongle und Thymio im gleichen Rhythmus rot blinken. Bringe den Thymio und den Dongle näher zueinander, bis dies der Fall ist.
  - Speichere die Konfiguration, indem du den Tyhmio ausschaltest.

3. Computer:
  - Drücke «Speichern im Dongle», um die neuen Einstellungen zu speichern.
