# Aktoren
---

Mit Aktoren kann der Roboter sich bemerkbar machen und seine Umgebung beeinflussen.

## Motor

Der Thymio-Roboter kann in erster Linie mit Hilfe der Räder bewegen. Die gewünschte Geschwindigkeit der Räder kann über die folgenden Variablen eingestellt werden:

| Variable             | Bedeutung                                   |
|:-------------------- |:------------------------------------------- |
| `motor.left.target`  | gewünschte Geschwindigkeit des linken Rads  |
| `motor.right.target` | gewünschte Geschwindigkeit des rechten Rads |

Es sind Werte von $-500$ bis $500$ möglich. Ein Wert von $500$ entspricht einer Geschwindigkeit von $20\text{ cm/s}$.

Mit `motor.left.speed` und `motor.right.speed` kann die aktuelle Geschwindigkeit der Räder abgelesen werden.

## Farbige Leuchtdioden

Der Thymio-Roboter hat drei farbige Leuchtdioden (engl. *light emitting diodes, LEDs*) eingebaut. Mit folgenden Anweisungen können sie gesteuert werden. Dabei stehen `r`, `g` und `b` für die Helligkeit der drei Grundfarben Rot, Grün und Blau. Ein Wert von `0` bedeutet ganz ausgeschaltet, ein Wert von `31` bedeutet maximale Helligkeit.

~~~ aseba
call leds.top(r, g, b)
~~~
steuert die Farbe auf der Oberseite des Thymio.

~~~ aseba
call leds.bottom.left(r, g, b)
~~~
steuert die Farbe auf der Unterseite links des Thymio.

~~~ aseba
call leds.bottom.right(r, g, b)
~~~
steuert die Farbe auf der Unterseite rechts des Thymio.


## Kreis-Leuchtdioden

~~~ aseba
call leds.circle(n, ne, e, se, s, sw, w, nw)
~~~
steuert die acht gelben Leuchtioden, welche im Kreis um das Tastenfeld angeordnet sind. Bei dieser Anweisung muss für jede der acht LEDs die Helligkeit zwischen `0` und `31` angegeben werden. Die erste Zahl `n` steht für die Leuchtdiode vorne (*north*), die weiteren folgen im Uhrzeigersinn.

## Leutchtioden bei Tasten

~~~ aseba
call leds.buttons(n, e, s, w)
~~~
steuert die Leuchtdioden, welche unter den vier Richtungstasten :thymio-up:,  :thymio-right:, :thymio-left: und :thymio-down: angebracht sind.

Bei dieser Anweisung muss für jede der vier LEDs die Helligkeit zwischen `0` und `31` angegeben werden. Die erste Zahl `n` steht für die Leuchtdiode vorne (*north*), die weiteren folgen im Uhrzeigersinn.

## Leuchtdioden bei Sensoren

~~~ aseba
call leds.prox.h(v0, v1, v2, v3, v4, v5, h0, h1)
~~~
steuert die roten Leuchtdioden, welche sich bei den Distanzsensoren befinden.

~~~ aseba
call leds.prox.v(h0, h1)
~~~
steuert die roten Leuchtdioden bei den Bodensensoren.


~~~ aseba
call leds.rc(h)
~~~
steuert die Leuchtdiode beim Empfänger der Fernsteuerung.

~~~ aseba
call leds.temperature(red, blue)
~~~
steuert die rote und blaue Leuchtdiode beim Temperatursensor.

~~~ aseba
call leds.sound(h)
~~~
steuert die blaue Leuchtdiode beim Mikrofon.

## Sound

~~~ aseba
call sound.system(n)
~~~
spielt einen vordefinierten Sound ab. Der Wert von `n` beschreibt den Sound, der abzuspielen ist:

| Wert | Bedeutung                                          |
|:---- |:-------------------------------------------------- |
| `-1` | Beendet das Abspielen eines Sounds                 |
| `0`  | Anschaltmelodie                                    |
| `1`  | Abschaltmelodie (dieser Ton ist nicht veränderbar) |
| `2`  | Ton der Pfeiltasten                                |
| `3`  | Ton der mittleren Taste                            |
| `4`  | Ton bei freiem Fall                                |
| `5`  | Ton bei einem Zusammenstoss                        |
| `6`  | Ziel ok bei freundlichem Verhalten                 |
| `7`  | Ziel entdeckt bei freundlichem Verhalten           |
