# Ereignisse
---

Aseba ist eine ereignisgesteuerte Programmiersprache.

``` aseba
onevent Ereignis
    Anweisungen
```

Im folgenden Beispiel werden die oberen farbigen Leuchtioden auf grün geschaltet, sobald die Taste :thymio-middle: gedrückt wird:

``` aseba
onevent button.center
    call leds.top(0, 32, 0)
```



## Regelmässige Ereignisse

Thymio liest die wichtigsten Sensorwerte regelmässig aus. Jedesmal, wenn das geschehen ist, wird ein entsprechendes Ereignis ausgelöst.

Die folgende Tabelle zeigt einer Übersicht der regelmässigen Ereignisse mit der Frequenz, in welcher sie auftreten:

| Ereignis                                   | Aseba         | Frequenz |
|:------------------------------------------ |:------------- | --------:|
| Tastenwerte sind abgelesen worden          | `buttons`     |    50 Hz |
| Distanzsensoren sind abgelesen worden      | `prox`        |    10 Hz |
| Beschleunigungsmesser ist abgelesen worden | `acc`         |    16 Hz |
| Temperatur ist abgelesen worden            | `temperature` |     1 Hz |

## Besondere Ereignisse

Daneben löst Thymio besondere Ereignisse aus, wenn eine besondere Situation eintritt:

| Ereignis                                    | Aseba             |
|:------------------------------------------- |:----------------- |
| Taste :thymio-down: gedrückt                | `button.backward` |
| Taste :thymio-left: gedrückt                | `button.left`     |
| Taste :thymio-right: gedrückt               | `button.right`    |
| Taste :thymio-up: gedrückt                  | `button.forward`  |
| Taste :thymio-middle: gedrückt              | `button.center`   |
| Erschütterung ist wahrgenommen worden       | `tap`             |
| Geräuschpegel hat Schwellwert überschritten | `mic`             |
| Audiowidergabe ist abgeschlossen worden     | `sound.finished`  |
| Fernbedienungssignal ist empfangen worden   | `rc5`             |
| Timer 0 ist abgelaufen                      | `timer0`          |
| Timer 1 ist abgelaufen                      | `timer1`          |
