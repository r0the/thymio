# Rechnen
---

## Wertebereich

Aseba kennt nur ganzzahlige Werte zwischen $-32768$ und $32767$. Mit allen anderen Zahlen (z.B. rationale Zahlen) kann Aseba nicht umgehen.

## Variable definieren

In Aseba müssen Variablen definiert werden, bevor sie benutzt werden können. Dabei sollte der Variable auch gleich ein Wert zugewiesen werden, ansonsten ist ihr Werte nicht definiert!

``` aseba
var x = 2
```

## Operationen

Python unterstützt die üblichen Grundoperationen:

| Operation      | Mathematisch                     | Python   |
|:-------------- |:-------------------------------- |:-------- |
| Addition       | $a + b$                          | `a + b`  |
| Subtraktion    | $a - b$                          | `a - b`  |
| Negation       | $-a$                             | `-a`     |
| Multiplikation | $a \cdot b$                      | `a * b`  |
| Division       | $\lfloor a/b\rfloor$             | `a / b`  |
| Modulo         | $a - \lfloor a/b\rfloor \cdot b$ | `a % b`  |
| absoluter Wert | $\vert a \vert$                  | `abs(a)` |

Das Resultat einer Division wird immer **abgerundet**.

## Inkrement und Dekrement

Aseba unterstützt auch die in Programmiersprachen üblichen Inrekement- und Dekrementoperatoren `++` und `--`, welche den Wert einer Variable um eins erhöhen beziehungsweise vermindern:

``` aseba
count++
x--
```

## Vektoren

Neben einfachen Variablen können in Aseba auch *Vektoren* definiert werden. Ein Vektor sind mehre Variablen, welche den gleichen Namen erhalten. Unterschieden werden sie durch einen *Index*.

Beispielsweise werden fünf Variablen benötigt, um Messwerte eine gewisse Zeit speichern zu können. Anstelle der fünf Variablen `a0`, `a1`, `a2`, `a3` und `a4` wird nun der Vektor `a[5]` definiert:

``` aseba
var a[5]
```

Die einzelnen Variablen können mit dem Index-Operator (eckige Klammern) angesprochen werden:

``` aseba
a[0] = 22
a[1] = a[0] / 2
a[2] = -3
a[3] = a[0] + a[2]
a[4] = 1
```

Dabei werden die Werte in aufeinanderfolgenden Speicherstellen abgelegt. Wie in der Informatik üblich wird mit dem Index 0 begonnen:

![](images/vector.svg)
