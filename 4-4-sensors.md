# Sensoren
---

Sensoren sind die «Sinne» des Roboters. Sämtliche Sensorwerte werden regelmässig abgefragt und in vordefinierten Variablen abgelegt.

## Tasten

Die folgenden Variablen geben Auskunft darüber, ob eine Taste gedrückt ist oder nicht.

| Variable          | Bedeutung             |
|:----------------- |:--------------------- |
| `button.backward` | Taste :thymio-down:   |
| `button.center`   | Taste :thymio-middle: |
| `button.forward`  | Taste :thymio-up:     |
| `button.left`     | Taste :thymio-left:   |
| `button.right`    | Taste :thymio-right:  |

Ein Wert von $1$ bedeutet, dass die Taste gedrückt ist, ein Wert von $0$, dass sie nicht gedrückt ist.

## Distanzsensoren

Diese Variablen enthalten die letzten Messungen der Distanzsensoren:

| Variable             | Bedeutung         |
|:-------------------- |:----------------- |
| `prox.horizontal[0]` | vorne ganz links  |
| `prox.horizontal[1]` | vorne links       |
| `prox.horizontal[2]` | vorne Mitte       |
| `prox.horizontal[3]` | vorne rechts      |
| `prox.horizontal[4]` | vorne ganz rechts |
| `prox.horizontal[5]` | hinten links      |
| `prox.horizontal[6]` | hinten rechts     |

Ein Wert von $0$ bedeutet, dass der Sensor nichts wahrnimmt, ein positiver Wert, dass ein Hindernis erkannt wird. Je grösser der Wert ist, desto näher ist das Hindernis.

## Bodensensoren

| Variable                | Bedeutung                                             |
|:----------------------- |:----------------------------------------------------- |
| `prox.ground.ambiant`   | Lichtstärke des Umgebungslichts                       |
| `prox.ground.reflected` | Lichtstärke bei eingeschalteter IR-Lichtquelle        |
| `prox.ground.delta`     | Differenz zwischen reflektiertem und umgebendem Licht |

## Beschleunigungsmesser

Thymio besitzt einen Beschleunigungmesser mit 3 Achsen. Die Liste acc enthält drei Variablen:

| Variable | Bedeutung                                            |
|:-------- |:---------------------------------------------------- |
| `acc[0]` | x-Achse (von rechts nach links, positiv nach links)  |
| `acc[1]` | y-Achse (von vorne nach hinten, positiv nach hinten) |
| `acc[2]` | z-Achse (von oben nach unten, positiv nach unten)    |

Die Werte des Beschleunigungsmessers liegen zwischen $-32$ und $32$. Ein Wert von $1$ entspricht einer Beschleunigung von $0.45\text{ N}$. Somit entspricht ein Wert von $23$ einer Beschleunigung von $1\text{ g}$. Die Werte werden mit einer Frequenz von $15\text{ Hz}$ aktualisiert.

## Timer

Wenn ein Timer gestartet worden ist, enthalten die entsprechenden Variablen die noch verbleibende Zeit im Millisekunden:

| Variable          | Bedeutung                                      |
|:----------------- |:---------------------------------------------- |
| `timer.period[0]` | verbleibende Zeit von Timer 0 im Millisekunden |
| `timer.period[1]` | verbleibende Zeit von Timer 1 im Millisekunden |

## Mikrofon

Der aktuelle Geräuschpegel, der vom eingebauten Mikrofon gemessen wird, wird in der Variable `mic.intensity` gespeichert.

Das `mic`-Ereignis wird nur dann ausgelöst, wenn der in `mic.threshold` festgelegte Pegel überschritten wird.

| Variable        | Bedeutung                                                  |
|:--------------- |:---------------------------------------------------------- |
| `mic.intensity` | aktueller Geräuschpegel                                    |
| `mic.threshold` | Schwellwert, bei welchem das Ereignis `mic` ausgelöst wird |

## Infrarot-Fernsteuerung

Der Thymio-Roboter kann Signale einer Infrarot-Fernsteuerung nach dem RC5-Protokoll empfangen. Ein solches Signal besteht immer aus einer Geräteadresse und einem Befehlscode.

Das zuletzt empfangene Signal wird in den folgenden Variablen gespeichert:

| Variable      | Bedeutung     |
|:------------- |:------------- |
| `rc5.address` | Geräteadresse |
| `rc5.command` | Befehlscode   |

## Temperatur

In der Variable `temperature` wird die aktuelle Innentemperatur des Roboters in Zehntel Grad Celsius gespeichert.
