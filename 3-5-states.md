# Zustände
---

Im fortgeschrittenen Modus können Zustände definiert und abgefragt werden. Jede der vier LEDs bei der Zustandsanzeige (zwischen den Tasten auf dem Roboter) kann eingeschaltet (orange), ausgeschaltet (weiss) oder ignoriert (grau) werden. Da die vier LEDs zusammen einen Zustand darstellen, sind 16 verschiedene Zustände möglich. Wird ein bestimmter Zustand eingestellt (mit der **blauen Zustandsaktion**), so wird dieser mit Hilfe der entsprechenden LEDs auf dem Roboter dargestellt.

Mit Hilfe der **grünen Zustandsabfrage** kann nun bei den Ereignissen erkannt werden, ob eine bestimmte (oder auch mehrere LEDs) eingeschaltet ist oder nicht.


## Ein einfaches Beispiel
Der Thymio leuchtet erst grün, wenn man die richtigen Tasten in der richtigen Reihenfolge antippt:

- Die Tasten :thymio-left: :thymio-right: müssen zu Beginn gleichzeitig gedrückt werden. Der Thymio wechselt dann in den nächsten Zustand (LED rechts oben leuchtet). Dies sieht man anhand der blauen Zustandsaktion.
- Anschliessend müssen die Tasten :thymio-up: und :thymio-down: gleichzeitig angetipp werden. Der Thymio wechselt dabei in den nachfolgenden Zustand (die LEDs rechts oben und rechts unten leuchten). Hätte man diese beiden Tasten gleich zu Beginn gedrückt, wäre nichts passiert, denn die Aktion findet nur statt, wenn sich der Thymio bereits im vorgegebenen Zustand (grüne Zustandsabfrage) befindet.
- Zum Abschluss muss noch die mittlere (runde) Taste :thymio-middle: angetippt werden, dann leuchtet der Thymio grün. Dies klappt aber nur, wenn beim Tastendruck die Zustands-LED unten rechts bereits leuchtet (wenn also die beiden vorherigen Tastenkombinationen gedrückt worden sind).
- Beim Einschalten der grünen Beleuchtung schaltet der Thymio auch gleich sämtliche Zustands-LEDs aus (weiss markierte Zustands-LEDs in der blauen Zustandsaktion).
- Die graue Farbe (in der grünen Zustandsabfrage resp. in der blauen Zustandsaktion) bedeutet, dass die entsprechende LED ignoriert wird.

![](images/example-states-1.png)


## Ein komplexeres Beispiel
Überlege, was das folgende Beispielprogramm tut:

![](images/example-states-2.png)

<span class="hidden">**Lösung**
Tippt man wiederholt auf die vorwärts-Taste, so leuchten die Zustands-LEDs nacheinander im Uhrzeigersinn auf.</span>

::: exercise Fragen

Was würde geschehen, wenn im ersten Block alle LEDs ignoriert (grau markiert) würden?

Was, wenn im ersten Block – wie bei den Blöcken zwei bis vier – nur eine bestimmte LED (also die vierte) als eingeschaltet geprüft würde?

Wieso wird im ersten Wenn-Dann-Block geprüft, ob drei bestimmte Zustands-LEDs ausgeschaltet sind?
***
#### Lösung
Würden im ersten Block alle LEDs ignoriert (grau eingestellt), dann würde stets – egal in welchem Zustand sich der Roboter befindet – die erste LED aktiviert und die letzte deaktiviert.

Wenn nur geprüft würde, ob die letzte LED leuchtet, dann könnte das Programm nie beginnen, da ja zu Beginn keine LED leuchtet.

Es muss also geprüft werden, ob die drei ersten LEDs ausgeschaltet sind, während es egal ist, ob die letzte LED leuchtet (nach einer Umdrehung) oder nicht (Programmstart).
:::
