# Installation auf Windows
---

## Herunterladen

Lade die neueste Version von Aseba hier herunter:

* [:download: Aseba 1.6.1](https://aseba.wdfiles.com/local--files/en:wininstall/aseba-1.6.1.exe)

## Installieren

Beim Installieren ist folgendes zu beachten:

- Der Installationstyp **ThymioII Package (Empfohlen)** muss ausgewählt werden.
- Der Installation der **Gerätesoftware** muss zugestimmt werden.

## Installieren im Detail

Hier ist eine Schritt-für-Schritt-Anleitung für die Installation:

![Klicke auf «Weiter»](images/install-1.png)

![Klicke auf «Annehmen»](images/install-2.png)

![Klicke auf «ThymioII Package (Empfohlen)»](images/install-3.png)

![Klicke auf «Weiter»](images/install-4.png)

![Klicke auf «Weiter»](images/install-5.png)

![Klicke auf «Installieren»](images/install-6.png)

![Klicke auf «Installieren»](images/install-7.png)
