# Grundlagen der Robotik
---

Die Robotik ist eine interdisziplinäre Wissenschaft, welche sich mit der Entwicklung und Steuerung von Robotern beschäftigt. Roboter sind Maschinen, welche automatisch komplexe Abfolgen von Aktionen ausführen können. Dazu müssen Roboter mit Hilfe von sogenannten **Aktoren** ihre Umgebung manipulieren können. Ausserdem benötigen sie **Sensoren**, um die Umgebung wahrnehmen zu können.

## Einsatzgebiete von Robotern

::: cards 2
![](images/curiosity.jpg)
#### Forschung

In der Forschung werden Roboter für vielfältige Aufgaben eingesetzt. Abgebildet ist der Mars-Rover **Curiosity**.

***
![](images/reaper.jpg)
#### Militär

Es werden immer mehr Roboter als autonome Waffensysteme eingesetzt, hier beispielsweise die **MQ-9 «Reaper»** des U.S.-Militärs in Afghanistan.

***
![](images/roomba.jpg)
#### Haushalt

Auch im Haushalt sind immer mehr Roboter anzutreffen, wie der Staubsaugerroboter **Roomba**.

***
![](images/industrial-robot.jpg)
#### Industrie

Am weitesten verbreitet sind nach wie vor die **Industrieroboter**.
:::
