# Ereignisse: einfach
---

## ![](images/event-buttons.svg) Tasten

Die Berührungstasten auf der Oberseite des Thymio können zur manuellen Steuerung des Roboters eingesetzt werden.

Bei diesem Ereignis muss eine der fünf Tasten ausgewählt werden. Ein Ereignis wird ausgelöst, wenn die entsprechende Taste berührt wird.


## ![](images/event-prox.svg) Distanzsensoren

Diese Sensoren werden eingesetzt, um Hindernisse und freie Wege zu erkennen.

Hier kann für jeden Sensor ein Detektionsmodus ausgewählt werden. Weiss bedeutet, dass der Sensor Objekte entdecken soll, Schwarz, dass der Sensor Leere entdecken soll, und Grau heisst, dass der Sensor ignoriert wird.

Wenn für mehrere Sensoren der Modus Weiss oder Schwarz gewählt wird, so wird ein Ereignis nur ausgelöst, wenn alle Bedingungen erfüllt sind.

## ![](images/event-ground.svg) Bodensensoren

Auch hier kann für jeden der beiden Sensoren ein Detektionsmodus ausgewählt werden. Weiss bedeutet, dass der Sensor eine vorhandene Oberfläche und Schwarz, dass der Sensor einen Abgrund entdecken soll. Grau heisst, dass der Sensor ignoriert wird.

Wenn für beide Sensoren der Modus Weiss oder Schwarz gewählt wird, so wird ein Ereignis nur ausgelöst, wenn beide Bedingungen erfüllt sind.

## ![](images/event-tap.svg) Beschleunigungssensor

Dieses Sensor entdeckt Erschütterungen des Thymios.


## ![](images/event-clap.svg) Mikrofon

Dieses Sensor kann Geräusche entdecken. Ein Ereignis wird ausgelöst, wenn die Lautstärke der Umgebungsgeräusche eine bestimmte Schwelle überschreitet.

Achtung: Der Sensor reagiert oft schon auf die Mototengeräusche des Thymios und kann deshalb in diesem Modus nicht sinnvoll verwendet werden.
