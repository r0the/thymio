# Ereignisse: erweitert[^1]
---

## ![](images/event-buttons-advanced.png) ![](images/event-rc-arrow.svg) ![](images/event-rc-keypad.svg) Tasten und Fernbedienung

Bei den Berührungssensoren kann über die drei Punkte auf die zusätzlichen Sensoren für die Fernbedienung gewechselt werden.

## ![](images/event-prox-advanced.svg) Distanzsensoren

Bei den Distanzsensoren kann nun über die beiden grauen Balken die Entfernung genauer angegeben werden, bei der das Ereignis auslösen soll. Der obere Balken gibt an, wie nahe das Objekt sein muss, damit es vom Sensor erkannt wird. Je kleiner der weisse Teil des Balkens ist, desto näher muss sich das Objekt am Sensor befinden. Der untere Balken gibt an, ab welcher Distanz ein Objekt nicht mehr «gesehen» wird. Je kleiner der schwarze Teil des Balkens ist, desto weiter sieht der Sensor die Objekte noch.

## ![](images/event-ground-advanced.svg) Bodensensoren

## ![](images/event-tap-advanced.png) ![](images/event-tilt.svg) ![](images/event-pitch.svg) Beschleunigungssensor

Bei den Beschleunigungssensoren kann neben einem «Antippen» des Roboters auch detektiert werden, ob er sich in einer schiefen Lage befindet.

## ![](images/event-timer.svg) Timer

Der klingelnde Wecker schliesslich wird ausgelöst, nachdem ein gesetzter Timer (siehe «Aktionen») abgelaufen ist. Um mehrere Timer-Ereignisse unterscheiden zu können, müssen Zustände gesetzt und überprüft werden (siehe «Zustände»).

[^1]: Quelle: Tom Jampen, [mygmer.ch](https://ict.mygymer.ch/lo/teach/exkurs-informatik/thymio/vpl-erweitert)
