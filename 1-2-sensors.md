# Sensoren
---

## Distanzsensoren

Die Distanzsensoren des Thymio verwenden Infrarotlicht (IR). Sie bestehen aus einer Infrarot-Leuchtdiode und einem Infrarot-Empfänger. Je näher sich ein Objekt befindet, desto mehr Infrarotlicht wird von diesem in Richtung Empfänger reflektiert.

![Funktionsweise der Distanzsensoren](images/distance-sensor.svg)

Die Menge des reflektierten Lichts hängt auch mit den Materialeigenschaften des Objekts zusammen. So reflektiert eine weisse Oberfläche mehr Sonnenlicht als eine schwarze. Analog geschieht das mit Infrarotlicht, nur dass man mit blossem Auge die «Weissheit» einer Oberfläche nicht erkennen kann.

Ausserdem reflektiert eine glatte Oberfläche mit gleichmässigen Winkeln, eine rauhe Oberfläche mit unregelmässigen. Damit verändert sich auch die Menge an Licht, welche in Richtung des Empfängers reflektiert wird.

**Fazit:** Ein Infrarotsensor misst also nicht die Distanz, sondern eine Kombination aus Distanz und Materialeigenschaften.


## Beschleunigungssensoren

Moderne Beschleunigungssensoren finden auf einem Mikrochip der Grösse 2×2 Millimeter platz. Für jede Richtung, in welcher die Beschleunigung gemessen werden soll, sind ein festes und ein flexibles Metallplättchen vorhanden.

Wegen der Trägheit biegt sich das flexible Plättchen bei einer Beschleunigung und ändert so seine Distanz zum festen Plättchen. Da die Plättchen unterschiedlich elektrisch geladen sind, ändert sich mit der Distanz auch deren **elektrische Kapazität**, welche gemessen werden kann.

![Funktionsweise eines Beschleunigungssensors"](images/accelerometer.svg)
