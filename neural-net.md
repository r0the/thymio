# Künstliche Intelligenz
---

## Künstliche Neuronen

$$ y = x_1 w_1 + x_2 w_2 + x3 w_3 + x4 w_4 + x5 w_5 $$

## Thymio mit künstlichen Neuronen

![](images/thymio-neural-net.svg)

$$ \begin{aligned}
y_1 &= x_1 w_1 + x_2 w_2 + x3 w_3 + x4 w_4 + x5 w_5 \\
y_2 &= x_1 v_1 + x_2 v_2 + x3 v_3 + x4 v_4 + x5 v_5
\end{aligned} $$

## Hebbsche Lernregel

Die Hebbsche Lernregel für das Lernen in künstlichen neuronalen Netzwerken lautet:

$$ \Delta w_i = \alpha \cdot x_i \cdot y $$

Dabei ist $\Delta w_i$ die Veränderung des Gewichts des Eingangs $i$, $\alpha$ ist die Lernrate, $x_i$ ist der an Eingang $i$ anliegende Wert und $y$ ist der gewünschte Ausgabewert.

## Musterlösung

``` aseba
var w2 = 1
var w3 = 0
var w4 = 0
var w5 = 0
var v1 = 0
var v2 = 0
var v3 = 0
var v4 = 0
var v5 = 0

var a = 1/1000

var y1 = 0
var y2 = 0

var x1 = 0
var x2 = 0
var x3 = 0
var x4 = 0
var x5 = 0

var mode = 0
call leds.top(0, 31, 0)

onevent buttons
	when button.center == 1 do
		if mode == 0 then
			mode = 1
			call leds.top(31, 0, 0)
		else
			mode = 0
			call leds.top(0, 31, 0)
		end
	end
	if mode == 1 then
		when button.forward == 1 do
			y1 = 100
			y2 = 100
		end
		when button.backward == 1 do
			y1 = -100
			y2 = -100
		end
		when button.left == 1 do
			y1 = -100
			y2 = 100
		end
		when button.right == 1 do
			y1 = 100
			y2 = -100
		end
	end

onevent prox
	x1 = prox.horizontal[0]
	x2 = prox.horizontal[2]
	x3 = prox.horizontal[4]
	x4 = prox.horizontal[5]
	x5 = prox.horizontal[6]

	if mode == 0 then
		y1 = x1*w1 + x2*w2 + x3*w3 + x4*w4 + x5*w5
		y2 = x1*v1 + x2*v2 + x3*v3 + x4*v4 + x5*v5
		motor.left.target = y1
		motor.right.target = y2
	else
		w1 = w1 + a * x1 * y1
		w2 = w2 + a * x2 * y1
		w3 = w3 + a * x3 * y1
		w4 = w4 + a * x4 * y1
		w5 = w5 + a * x5 * y1

		v1 = v1 + a * x1 * y2
		v2 = v2 + a * x2 * y2
		v3 = v3 + a * x3 * y2
		v4 = v4 + a * x4 * y2
		v5 = v5 + a * x5 * y2
	end
```
