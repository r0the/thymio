# Aktionen: einfach
---

## ![](images/action-motors.svg) Motoren


## ![](images/action-colors-top.svg) LED-Farbe oben

Mit dieser Aktion wird die Farbe der Leuchtdiode, welche den oberen Teil des Roboters erhellt, eingestellt. Die gewünschte Farbe wird nach dem bekannten RGB-Farbmodell aus den Grundfarben Rot, Grün und Blau gemischt. Die Anteile der Grundfarben werden über die Schieberegler eingestellt. Die Position ganz links bedeutet kein Anteil dieser Farbe, die Position ganz rechts ein maximaler Anteil.

Das Andern der Farbe kann als hilfreiche visuelle Rückmeldung eingesetzt werden. So kann einfach kontrolliert werden, ob bestimmte Ereignisse wie vermutet eintreffen.

## ![](images/action-colors-bottom.svg) LED-Farbe unten

Mit dieser Aktion wird die LED-Farbe auf der Unterseite des Thymios eingestellt.

## ![](images/action-music.svg) Tonwiedergabe

Für die Tonwiedergabe können die Punkte auf eine der fünf zur Verfügung stehenden Tonhöhen gesetzt werden. Die Melodie darf höchstens aus sechs Tönen bestehen, wobei ein schwarzer Punkt für einen kurzen Ton, ein weisser für einen langen Ton steht.
