# Timer
---

In Aseba stehen zwei Timer zu Verfügung, mit welchen verzögerte und regelmässig wiederholte Aktionen programmiert werden können.

## Funktionsweise

Pro Timer gibt es eine Variable, welche die noch verbleibende Zeit im Millisekunden enthält:

| Variable          | Bedeutung                                      | Ereignis |
|:----------------- |:---------------------------------------------- |:-------- |
| `timer.period[0]` | verbleibende Zeit von Timer 0 im Millisekunden | `timer0` |
| `timer.period[1]` | verbleibende Zeit von Timer 1 im Millisekunden | `timer1` |

Um einen Time zu starten, wird die entsprechende Variable auf die gewünschte Zeitdauer in Millisekunden gesetzt:

``` aseba
timer.period[0] = 500
```

Die Timervariablen werden automatisch aktualisiert. Sobald sie den Wert 0 erreichen, wird das Timer-Ereignis `timer0` bzw. `timer1` ausgelöst.

``` aseba
onevent timer0
    # Aktion
```

## Verzögerte Aktion

Eine typische Anwendung für Timer ist eine verzögerte Aktion. Beispielsweise soll der Thymio nach Drücken der Taste :thymio-up: eine Sekunde lang geradeaus fahren, dann anhalten.

``` aseba
onevent button.forward
    motor.left.target = 300
    motor.right.target = 300
    timer.period[0] = 1000

onevent timer0
    motor.left.target = 0
    motor.right.target = 0
```

## Regelmässig wiederholte Aktion

Auch regelmässig wiederholte Aktionen können mit einem Timer realisiert werden. Im folgenden Beispiel soll der Thymio im Sekundentakt blinken, also pro Sekunde einmal ein- und ausgeschaltet werden. Dazu muss das Ereignis `timer0` pro Sekunde zwei Mal, also mit einer Frequenz von 2 Hz, auftreten.

``` aseba
var x = 0
timer.period[0] = 1

onevent timer0
    if x == 0 then
        x = 31
    else
        x = 0
    end
    call leds.top(x, x, x)
    timer.period[0] = 500
```

Die Anweisung `timer.period[0] = 1` sorgt dafür, dass beim Start des Programms das Ereignis ein erstes Mal auftritt.

Falls die gewünschte Frequenz 1, 10, 16 oder 50 Hz beträgt, kann anstelle eines Timers einfach das entsprechende regelmässige Ereignis verwendet werden.
