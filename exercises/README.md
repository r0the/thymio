# Aufgaben auf PPT-Präsentation
---

## Aufgabe 1

Spielen Sie bei jedem Tastendruck einen anderen Ton und wechseln Sie die Farbe.

## Aufgabe 2

Lassen Sie den Roboter im Kreis fahren. Start und Stop auf Tastendruck.

## Aufgabe 3

Weichen Sie nach links resp. rechts aus, wenn aussen rechts resp. links ein Hindernis auftaucht.

## Aufgabe 4

Lassen Sie den Roboter auf Tastendruck ein Dreieck fahren und dann anhalten.
