* Spiralenförmige Bewegungen wären ideal, um den Raum zu erforschen, dies
  können wir nicht machen, weil die Geschwindigkeit nur bei Ereignissen
  geändert werden kann.

* Der Roboter kann nicht von der Tischkante rückwärts fahren und dann drehen.

* Wenn er solange rückwärts fährt, bis der Sensor nicht mehr den Abgrund sieht,
  reicht es nicht zum Wegdrehen.

* Neugieriges Verhalten ist also schwierig, da wir noch keine Timer und
  Zustände haben!

