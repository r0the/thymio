# Kontrollstrukturen
---

Mit Kontrollstrukturen kann der Ablauf eines Programms gesteuert werden.

## Bedingungen

Die meisten Kontrollstrukturen benötigen eine **Bedingung**. Gewisse Anweisungen werden nur dann ausgeführt oder wiederholt, wenn die Bedingung erfüllt ist.

Eine Bedingung ist ein Vergleich zweier Werte. In Aseba können die üblichen mathematischen Vergleiche so dargestellt werden:

| Vergleich               | Mathematisch | Python   |
|:----------------------- |:------------ |:-------- |
| kleiner als             | $a < b$      | `a < b`  |
| kleiner als oder gleich | $a \leq b$   | `a <= b` |
| grösser als             | $a > b$      | `a > b`  |
| grösser als oder gleich | $a \geq b$   | `a >= b` |
| gleich                  | $a = b$      | `a == b` |
| nicht gleich            | $a \neq b$   | `a != b` |

::: warning
#### :warning: Gleichheit und Zuweisung
Die Gleichheit wird durch zwei aufeinanderfolgende Gleichheitszeichen `==` dargestellt. Ein einzelnes Gleichheitszeichen `=` wird für die Zuweisung eines Werts an eine Variable verwendet.
:::

Mit Hilfe von `and`, `or` und `not` können Vergleich auch zu komplexeren Bedingungen verknüpft werden:

| Operation | Python | Beispiel            |
|:--------- |:------ |:------------------- |
| Oder      | `or`   | `x < 1 or x > 5`    |
| Und       | `and`  | `x >= 1 and x <= 5` |
| Nicht     | `not`  | `not x > 10`        |

## Bedingte Anweisung und Verzweigung

Wenn eine oder mehrere Anweisungen nur manchmal ausgeführt werden sollen, kann eine **bedingte Anweisung** verwendet werden. Eine solche Anweisung beginnt mit dem `if`, gefolgt von einer Bedingung und `then`. Auf den nächsten Zeilen folgen die Anweisung(en), welche nur ausgeführt werden sollen, wenn die Bedingung erfüllt ist. Diese sollten gegenüber der Bedingung eingerückt werden. Der Block wird mit einem `end` abgeschlossen.

``` aseba
if Bedingung then
    Anweisungen
end
```

Im folgenden Beispiel werden die Motoren eingeschaltet, falls der Wert der Variable `button.forward` gleich 1 ist:

``` aseba
if button.forward == 1 then
    # wird ausgeführt, falls x grösser als 5 ist
    motor.left.target = 150
    motor.right.target = 150
end
```

## Verzweigung

Es ist auch möglich mehrere Bedingungen aneinanderzureihen. Dazu wird mit `elseif` eine weitere Bedingung eingefügt. Mit `else` können Anweisungen definiert werden, welche ausgeführt werden sollen, falls keine der Bedingungen zutrifft.

``` aseba
if x > 5 then
    # wird ausgeführt, falls x grösser als 5 ist
elseif x > 3 then
    # wird ausgeführt, falls x gleich 4 oder 5 ist
else
    # wird ausgeführt, falls x kleiner oder gleich 3 ist
end
```

## Bedingte Anweisung bei Änderung

Manchmal sollte eine Anweisung nur das erste Mal ausgeführt werden, wenn eine bestimmte Bedingung eintritt. Dazu kann das folgende Konstrukt verwendet werden:

``` aseba
when Bedingung do
    Anweisungen
end
```

Das folgende Beispiel zählt in der Variable `count`, wie oft die Vorwärts-Taste des Roboters gedrückt wird.

``` aseba
var count = 0

onevent buttons
    when button.forward == 1 do
        count = count + 1
    end
```

Mit der «normalen» bedingten Anweisung `if` würde hier `count` weitergezählt, solange die Taste gedrückt bleibt.

## Vorprüfende Schleife

Mit einer vorprüfenden Schleife können Anweisungen solange wiederholt werden, bis eine Bedingung eintritt. Eine solche Schleife hat folgende Struktur:

``` aseba
while Bedingung do
    Anweisungen
end
```

## Zählschleife

Eine Zählschleife ermöglicht es, Anweisungen eine bestimmte Anzahl mal auszuführen. Eine Zählvariable, üblicherweise `i`, wird verwendet, um die Durchläufe zu zählen.

``` aseba
var i
for i in 0:9 do
    Anweisungen
end
```

Zählschleifen werden typischerweise eingesetzt, um eine Anweisung mit allen Elementen eines Vektors durchzuführen. Im folgenden Beispiel wird mit Hilfe einer Zählschleife die Summe der Werte im Vektor `a` gebildet:

``` aseba
var a[10] = [8, 4, 7, 9, 3, 2, 9, 5, 4, 7]
var i
var sum = 0
for i in 0:9 do
      sum = sum + a[i]
end
```
