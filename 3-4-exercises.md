# Aufgaben Timer
---

## Arbeiten mit dem Timer
1. Der Thymio soll auf Tastendruck losfahren. Sobald die Taste nach rechts gedrückt wird, soll der Thymio eine 90°-Kurve nach rechts und anschliessend automatisch wieder geradeaus fahren.
1. Der Thymio soll auf Tastendruck losfahren. Sobald er vor sich ein Hindernis sieht, soll er für 1s lang eine Rückwärtskurve fahren und anschliessend wieder geradeaus fahren.
