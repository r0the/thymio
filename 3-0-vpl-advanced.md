# VPL: erweitert
---

Klickt man in der Symbolleiste oben auf die Person mit dem Hut, wechselt die Oberfläche zwischen der einfachen und der fortgeschrittenen Ansicht hin und her.

Die erweiterte VPL-Oberfläche bietet ein paar neue Ereignisse und Aktionen, sowie einige zusätzliche Einstellungen für bestehende Eregnisse und Aktionen:

![Übersicht Thymio VPL im erweiterten Modus ©](images/vpl-gui-advanced.svg)
