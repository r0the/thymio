# Aufgaben Zustände
---

## Arbeiten mit Zuständen
1. Baue das Programm mit dem einzutippenden Code so aus, dass der Thymio immer **rot** leuchtet, wenn der Code in einer falschen Reihenfolge eingegeben wird. Wenn mit der richtigen Kombination begonnen oder weitergefahren wird, soll der Thymio die rote Farbe wieder ausschalten.
1. Der Roboter soll jeweils automatisch nach einer Sekunde seine Farbe ändern (**rot** ‣ **grün** ‣ **blau** ‣ **weiss** und von vorne). Arbeite auch mit dem Timer, damit du die aktuelle Farbe jeweils eine Sekunde sehen kannst.
